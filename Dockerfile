FROM ubuntu:latest

RUN apt-get update                    \
 && apt-get -y install openjdk-11-jdk \
 && apt-get -y install postgresql-12  \
 && apt-get -y install npm            \
 && apt-get -y install curl           \
 && apt-get -y install git            \
 && apt-get install -y ruby-dev       \
 && gem install dpl-heroku

CMD /bin/bash
